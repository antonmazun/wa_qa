
from django.urls import path , include

from .views import (home_page ,
	detail_book ,
	search ,
	detail_author)

urlpatterns = [
    path('' , home_page),
	path('book-detail/<int:article_id>/' , detail_book ),
	path('about-author/<int:author_id>/' , detail_author ),
	path('search/' , search ),
]
