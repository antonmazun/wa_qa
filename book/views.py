from django.shortcuts import render
from .models import Book , Author


# Create your views here.


def home_page(request):
    all_books = Book.objects.all() # select * from book_book;
    genres  = Book.GENRE_CHOICE
    return render(request, 'book/main.html', {
        'books': all_books,
        'genres': genres,
    })



def detail_book(request , article_id):
    ctx  = {}
    book  = Book.objects.get(id=article_id)
    ctx['book'] = book
    return render(request , 'book/detail_book.html' , ctx)


def detail_author(request , author_id):
    ctx  = {}
    author  = Author.objects.get(pk=author_id)
    # all_books = author.get_books()
    # filter_book = [book for book in all_books if book.is_sale]
    # print('filter_book ' , filter_book)
    ctx['author'] = author
    # ctx['all_books'] = all_books
    return render(request , 'book/detail_author.html' , ctx)


def search(request):
    ctx  = {}
    genre = request.GET.get('genre')
    from_price = int(request.GET.get('from_price'))
    to_price = int(request.GET.get('to_price'))
    books = Book.objects.filter(genre=genre)
    range_price  = range(from_price , to_price+1)
    new_list_book = []
    for book in books:
        if book.is_sale:
            if round(book.new_price) in range_price:
                new_list_book.append(book)
        else:
            if round(book.price) in range_price:
                new_list_book.append(book)
    ctx['books'] = new_list_book
    # ctx['new_list_book'] = new_list_book
    ctx['genres'] = Book.GENRE_CHOICE
    return render(request , 'book/main.html' , ctx)