# Generated by Django 2.2 on 2020-04-07 16:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('book', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='book',
            name='genre',
            field=models.CharField(choices=[('comedy', 'Комедия'), ('mistika', 'Мистика')], max_length=15, null=True),
        ),
        migrations.AddField(
            model_name='book',
            name='is_sale',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='book',
            name='new_price',
            field=models.FloatField(default=0),
        ),
    ]
