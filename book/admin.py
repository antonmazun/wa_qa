from django.contrib import admin
from .models import Book , Author

# Register your models here.



class BookAdmin(admin.ModelAdmin):
	list_display = ['id' , 'title'  , 'price' , 'new_price' , 'is_sale' , 'author' , 'genre']
	list_editable = ['title' , 'price' , 'new_price' , 'is_sale' , 'author'  ,'genre']
	list_filter = ['is_sale']

class AuthorAdmin(admin.ModelAdmin):
	list_display = ['id' , 'name' , 'surname']
	list_editable = ['name' , 'surname']

admin.site.register(Book , BookAdmin)
admin.site.register(Author , AuthorAdmin)