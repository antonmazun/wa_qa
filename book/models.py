from django.db import models


# Create your models here.

class Author(models.Model):
	name = models.CharField(max_length=255, verbose_name='Имя автора')
	surname = models.CharField(max_length=255, verbose_name='Фамилия автора')
	nickname = models.CharField(max_length=255, verbose_name='Никнейм')
	date_birth = models.DateField()
	date_dead = models.DateField(blank=True, null=True)  # NULL

	def __str__(self):
		return f'{self.id} : {self.surname} {self.name}'

	def get_full_name(self):
		return '{name} {surname}'.format(name=self.name.upper() , surname=self.surname.upper())


	def get_books(self):
		return Book.objects.filter(author=self)

	def is_sales_books(self):
		return self.get_books().filter(is_sale=True)

	def is_not_sales_books(self):
		return self.get_books().filter(is_sale=False)

	def get_total_price(self):
		return sum([book.new_price for book in self.is_sales_books()] + \
	[book.price for book in self.is_not_sales_books()])

	def get_total_sales(self):
		all_price_is_sale  = sum([book.new_price for book in self.is_sales_books()])
		all_price_books  = sum([book.price for book in self.get_books()])
		return all_price_books - all_price_is_sale


class Book(models.Model):
	GENRE_CHOICE = (
		('comedy', 'Комедия'),
		('mistika', 'Мистика')
	)
	author = models.ForeignKey(Author, on_delete=models.CASCADE, blank=True, null=True)
	title = models.CharField(max_length=100)
	price = models.FloatField()
	description = models.TextField(max_length=100)
	is_sale = models.BooleanField(default=False)
	new_price = models.FloatField(default=0)
	genre = models.CharField(choices=GENRE_CHOICE, max_length=15, null=True)  # NULL


	def sales(self):
		if self.is_sale:
			return self.price - self.new_price

	def __str__(self):
		return 'title - {} price - {}'.format(self.title, self.price)
