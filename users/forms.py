from django.contrib.auth.forms import UserCreationForm
from django import forms
from .models import UserAuthor
from django.contrib.auth.models import User


class SignUpForm(UserCreationForm):
	first_name  = forms.CharField(max_length=100 , label='Имя' , required=True)
	last_name  = forms.CharField(max_length=100 , label='Фамилия' , required=True)
	email = forms.EmailField(max_length=255)


	def save(self , *args , **kwargs):
		data  = self.cleaned_data
		django_user  = User.objects.create_user(
			username=data['username'],
			password=data['password1'],
			first_name =data['first_name'],
			last_name =data['last_name'],
			email=data['email'],
			is_active  = True
		)
		UserAuthor.objects.create(user=django_user)
		print('save custom method!!!')
		return django_user
