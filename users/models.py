from django.contrib.auth.models import User
from django.db import models


# Create your models here.


class UserAuthor(models.Model):
	TYPE_CHOICE = (('fio', 'ФИО'), ('nickname', 'Псевдоним'),)
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	bio = models.TextField(max_length=1500, blank=True, null=True)
	nickname = models.CharField(max_length=255, blank=True, null=True)
	type_user_view = models.CharField(choices=TYPE_CHOICE,
		max_length=len('nickname'),
		blank=True, null=True)

	__str__ = lambda self: self.user.username

	class Meta:
		verbose_name = 'Автор'
		verbose_name_plural = 'Авторы'
