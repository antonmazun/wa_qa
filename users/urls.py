from django.urls import path , include
from .auth_views import login_ , register_ , logout_
app_name  = 'client_app'



urlpatterns = [
   path('login/' , login_ , name='login'),
   path('register/' , register_ , name='register'),
   path('logout/' , logout_ , name='logout'),
]
