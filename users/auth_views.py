from django.shortcuts import render , redirect
from .forms import SignUpForm
from django.contrib.auth import authenticate , logout , login



def login_(request):
	ctx  = {}
	if request.method  == 'POST':
		username  = request.POST.get('username')
		password  = request.POST.get('password')
		user  = authenticate(username=username , password=password)
		if user:
			login(request , user)
			return redirect('/')
		else:
			ctx['error'] = 'Неправильный логин или пароль.'
	return render(request , 'users/auth/login.html' , ctx)



def register_(request):
	ctx  = {}
	ctx['form'] = SignUpForm()
	if request.method == 'POST':
		form = SignUpForm(request.POST)
		if form.is_valid():
			django_user = form.save()
			user  = authenticate(username=django_user.username ,
				password=form.cleaned_data['password2'])
			login(request , user)
			return redirect('/')
		else:
			ctx['form'] = SignUpForm(request.POST)
		print('post request!!')
	return render(request , 'users/auth/register.html' , ctx)



def logout_(request):
	logout(request)
	return redirect('client_app:login')
