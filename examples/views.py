from django.shortcuts import render

# Create your views here.


calc_obj = {
	'+': lambda x, y : x + y,
	# '-': lambda x, y : x - y,
	# '*': lambda x, y : x * y,
	'/': lambda x, y : x / y
}

def calc_page(request):
	ctx = {'tab': 'calc'}
	ctx['operations'] = calc_obj.keys()
	try:
		first_number  = float(request.GET.get('first_number'))
		oper  = request.GET.get('oper').strip()
		second_number  = float(request.GET.get('second_number'))
		result  = calc_obj[oper](first_number , second_number)
		ctx['result'] = f'{first_number} {oper} {second_number} = {result}'
	except Exception as e:
		ctx['error'] = e
		print(type(e) , e)
	return render(request , 'examples/calc_page.html' , ctx)


import requests as R

API_URL  = 'https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5'
def exchange(request):
	ctx  = {}
	currency = request.POST.get('currency')
	count = float(request.POST.get('count'))
	print(currency , count)
	response  = R.get(API_URL).json()
	for res in response:
		if res.get('ccy') == currency:
			result = float(res.get('sale')) * count
			ctx['result'] = f"Вот столько {round(result,2)}  грн тебе нужно потратить на покупку"
			break
	return render(request , 'examples/calc_page.html' , ctx)

from .forms import ExampleForm

def test_form(request):
	ctx = {}
	ctx['form'] = ExampleForm()
	# if request.method  == 'GET':
	if request.method == 'POST':
		form  = ExampleForm(request.POST , request.FILES)
		if form.is_valid():
			form.save()
		else:
			ctx['form'] = ExampleForm(request.POST , request.FILES)
	return render(request , 'examples/test_form.html' , ctx)