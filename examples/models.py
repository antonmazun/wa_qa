from django.db import models


# Create your models here.

class Example(models.Model):
	CHOICE = (('1', 'option 1'), ('2', 'option 2'), ('3', 'option 3'),)
	int_field = models.IntegerField()
	float_field = models.FloatField()
	date_field = models.DateTimeField()
	char_field = models.CharField(max_length=255)
	text_field = models.TextField(max_length=1500)
	choice_field = models.CharField(choices=CHOICE , max_length=3)
	image_field = models.ImageField(upload_to='images/')
	file_field = models.FileField(upload_to='files/')
	bool_field = models.BooleanField()

	def __str__(self):
		return self.char_field

