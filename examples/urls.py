
from django.urls import path , include
from .views import calc_page , exchange , test_form

app_name  = 'example'

urlpatterns = [
    path('calculator/' , calc_page , name='calc'),
    path('exchange/' , exchange),
    path('asdasdasd-asdasdasdasd/' , test_form , name='form')
]
